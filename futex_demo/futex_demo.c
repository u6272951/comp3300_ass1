#define _GNU_SOURCE

#include <stdio.h>
#include <linux/futex.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdatomic.h>

#define MAX_ITER 50

#define handle_error(msg)   \
    do                      \
    {                       \
        perror(msg);        \
        exit(EXIT_FAILURE); \
    } while (0)

static int cmpxchg(atomic_int *reg, int expected, int desired)
{
    return atomic_compare_exchange_strong(reg, &expected, desired);
}

static void ftx_wait(atomic_int *ftx_ptr)
{
    while (1)
    {
        // Clears futex if its set & exits
        // A set futex means that its available, and will dodge the syscall
        if (cmpxchg(ftx_ptr, 1, 0))
            break;

        // If futex value is unavailable, sleeps
        int rc = syscall(SYS_futex, ftx_ptr, FUTEX_WAIT, 0, 0, 0, 0);
        if (rc == -1)
            handle_error("futex wait");
    }
}

static int ftx_post(atomic_int *ftx_ptr)
{
    // If futex is unavailable, mark it as available
    // & wakes up waiters
    if (cmpxchg(ftx_ptr, 0, 1))
    {
        int rc = syscall(SYS_futex, ftx_ptr, FUTEX_WAKE, 1, 0, 0, 0);
        if (rc == -1)
            handle_error("futex wake");
    }
}

int main(int argc, char const *argv[])
{
    atomic_int *ftx1, *ftx2;

    // Shares futexes across processes
    atomic_int *addr = mmap(NULL, sizeof(atomic_int) * 2, PROT_READ | PROT_WRITE,
                            MAP_ANONYMOUS | MAP_SHARED, -1, 0);
    if (addr == MAP_FAILED)
        handle_error("mmap");

    // 0 := unavailable
    // 1 := available
    // Usually we want a state "2" to show a contended state
    ftx1 = &addr[0]; *ftx1 = 0;
    ftx2 = &addr[1]; *ftx2 = 1;

    switch (fork())
    {
    case -1:
        handle_error("fork");

    case 0:
        for (int i = 0; i < MAX_ITER; ++i)
        {
            ftx_wait(ftx1);
            printf("CHILD WRITES\t: %d\n\n", i);
            ftx_post(ftx2);
        }
        _exit(EXIT_SUCCESS);

    default:
        for (int i = 0; i < MAX_ITER; ++i)
        {
            ftx_wait(ftx2);
            printf("PARENT WRITES\t: %d\n", i);
            ftx_post(ftx1);
        }
        wait(NULL);
        munmap(addr, sizeof(int) * 2);
        exit(EXIT_SUCCESS);
    }
}