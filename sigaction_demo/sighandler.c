#define _GNU_SOURCE

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define handle_error(msg)   \
    do                      \
    {                       \
        perror(msg);        \
        exit(EXIT_FAILURE); \
    } while (0)

static void sigsegv_handler(int sig, siginfo_t *info, void *ucontext)
{
    printf("Inside the signal handler\n");
    printf("Exception number: %d\n", sig);
    printf("Will exit with success\n");
    exit(EXIT_SUCCESS);
}

int main()
{
    // Sets up the signal handler
    struct sigaction sigact;

    memset(&sigact, 0, sizeof(struct sigaction));
    sigemptyset(&sigact.sa_mask);

    sigact.sa_flags = SA_SIGINFO;
    sigact.sa_sigaction = sigsegv_handler;

    if (sigaction(SIGSEGV, &sigact, NULL) == -1)
        handle_error("sigaction");

    printf("Waiting for SIGSEGV signal...\n\n");
    fflush(stdout);

    while (1)
        sleep(1);

    printf("Success\n");
    exit(EXIT_SUCCESS);
}
